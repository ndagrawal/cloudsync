package imagegallery.example.com.cloudsync;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.FragmentManager;
import android.app.ListActivity;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.IntentSender;
import android.content.IntentSender.SendIntentException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract.Contacts;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.Media;
import android.provider.Telephony.Sms;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveApi.ContentsResult;
import com.google.android.gms.drive.MetadataChangeSet;

public class ServiceClient extends ListActivity implements communicator, ConnectionCallbacks, OnConnectionFailedListener{

	private static final String TAG = "ImageServiceClient";
	DatabaseManagerAdapter databaseManagerAdapter;	
	public ListView lv;
	ArrayList<Data> dataSet;
	PolicyListAdapter arrayAdap;
	private GoogleApiClient mGoogleApiClient;
	private Cursor cursor;
	private Uri uri;
	private String select;
	private String[] selectionArgs;
	private String sortOrder;
	private static final int REQUEST_CODE_RESOLUTION = 3;
	private static final int REQUEST_CODE_CREATOR = 2;
	//private static final int ;
	/* Understand the android activity life cycle : 
	 * onCreate->OnStart->onResume.
	 */

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		/* Setting the view for the activity*/
		setContentView(R.layout.image_service_client);
		/*Initialise the databaseManagerAdapter */
		databaseManagerAdapter = new DatabaseManagerAdapter(this);	
		/* Get the reference of the ListView*/
		lv = (ListView)findViewById(android.R.id.list);
		dataSet = new ArrayList<Data>();
		arrayAdap = new PolicyListAdapter(this, dataSet);
		lv.setAdapter(arrayAdap);
	}


	/*Singleton Pattern*/
	private static ServiceClient mInstance;
	public static final ServiceClient getInstance(){
		if(mInstance==null){
			mInstance = new ServiceClient();
		}
		return mInstance;
	}

	/*Constructor*/
	public ServiceClient(){

	}


	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		if (mGoogleApiClient == null) {
			// Create the API client and bind it to an instance variable.
			// We use this instance as the callback for connection and connection
			// failures.
			// Since no account name is passed, the user is prompted to choose.
			mGoogleApiClient = new GoogleApiClient.Builder(this)
			.addApi(Drive.API)
			.addScope(Drive.SCOPE_FILE)
			.addScope(Drive.SCOPE_APPFOLDER)
			.addConnectionCallbacks(this)
			.addOnConnectionFailedListener(this)
			.build();
		}
		// Connect the client. Once connected, the camera is launched.
		mGoogleApiClient.connect();
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		switch (requestCode) {

		case REQUEST_CODE_CREATOR:
			// Called after a file is saved to Drive.
			if (resultCode == RESULT_OK) {
				Log.i(TAG, "Image successfully saved.");
				/*mBitmapToSave = null;
                    // Just start the camera again for another photo.
                    startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE),
                            REQUEST_CODE_CAPTURE_IMAGE);
				 */
			}
			break;
		}
		if (requestCode == REQUEST_CODE_RESOLUTION && resultCode == RESULT_OK) {
			mGoogleApiClient.connect();
		}
	}

	private void SaveImagesToDrive(){

		Drive.DriveApi.newContents(mGoogleApiClient).setResultCallback(new ResultCallback<DriveApi.ContentsResult>() {

			@Override
			public void onResult(DriveApi.ContentsResult result) {
				// TODO Auto-generated method stub

				if (!result.getStatus().isSuccess()) {
					Log.i(TAG, "Failed to create new contents.");
					return;
				}


				//Uri :
				uri = Media.EXTERNAL_CONTENT_URI;
				String[] filePathColumn = { MediaStore.Images.Media.DATA };

				Cursor cursor = getContentResolver().query(uri,filePathColumn, null, null, null);
				int i=0;
				if(cursor!=null && !cursor.moveToNext()){
					String picturePath = cursor.getString(cursor.getColumnIndex( MediaStore.Images.Media.DATA ));
					Bitmap bp = BitmapFactory.decodeFile(picturePath);
					OutputStream outputStream = result.getContents().getOutputStream();
					// Write the bitmap data from it.
					ByteArrayOutputStream bitmapStream = new ByteArrayOutputStream();

					try {
						outputStream.write(bitmapStream.toByteArray());
					} catch (IOException e1) {
						Log.i(TAG, "Unable to write file contents.");
					}
					// Create the initial metadata - MIME type and title.
					// Note that the user will be able to change the title later.
					MetadataChangeSet metadataChangeSet = new MetadataChangeSet.Builder()
					.setMimeType("image/jpeg").setTitle("Android Photo.png"+i).build();
					// Create an intent for the file chooser, and start it.
					IntentSender intentSender = Drive.DriveApi
							.newCreateFileActivityBuilder()
							.setInitialMetadata(metadataChangeSet)
							.setInitialContents(result.getContents())
							.build(mGoogleApiClient);

					try {
						startIntentSenderForResult(
								intentSender, REQUEST_CODE_CREATOR, null, 0, 0, 0);
					} catch (SendIntentException e) {
						Log.i(TAG, "Failed to launch file chooser.");
					}
					i++;
				}
				cursor.close();

				//ImageView imageView = (ImageView) findViewById(R.id.imgView);
				//imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));
			}

		});
	}

	private void saveContactsToDrive(){
		/*Step no 1: To Retrive all the contacts using Content Resolver and  URI*/
		//URI: 

		Drive.DriveApi.newContents(mGoogleApiClient).setResultCallback(new ResultCallback<ContentsResult>() {	


			@Override
			public void onResult(ContentsResult result) {

				// If the operation was not successful, we cannot do anything
				// and must
				// fail.
				if (!result.getStatus().isSuccess()) {
					Log.i(TAG, "Failed to create new contents.");
					return;
				}

				/* Get the contacts from the mobile. */
				// TODO Auto-generated method stub
				uri = Contacts.CONTENT_URI;
				//String[] Projection, Column To be Extracted Names. 
				String[] projection = {Contacts._ID,Contacts.DISPLAY_NAME,Contacts.PHOTO_THUMBNAIL_URI};


				//Selection: Filte to het the perfect subset.
				//This will remove the null contacts for me.
				String whereClause =null;
				//"(("+Contacts.DISPLAY_NAME+ " NOTNULL) AND (" + Contacts.DISPLAY_NAME +" !='') AND (" + Contacts.STARRED + "==1))";

				//String selectionArgs = The subset of data we want to select from everything. 
				//Keep null if you want everything.s
				String[] selectionArgs = null;

				//String sortOrder = The way you wish your data to should be sorted.
				String sortOrder = Contacts._ID +" ASC";

				//Get the content Resolver.
				ContentResolver contentResolver = getContentResolver();
				//Use the queryMethod from the contentResolver to work on it.
				cursor = contentResolver.query(uri, projection,whereClause, selectionArgs, sortOrder);

				//this method will give the cursor, which can help us iterate through long list.
				//Set the ListAdapter.
				/* Convert those entries into the json format */
				JSONArray jsonArray = new JSONArray();

				while(cursor.moveToNext()!=false){
					JSONObject jsonObject = new JSONObject();
					Log.d("Contacts",cursor.getString(cursor.getColumnIndex(Contacts.DISPLAY_NAME)));
					try {

						jsonObject.put("name", cursor.getString(cursor.getColumnIndex(Contacts.DISPLAY_NAME)));
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					jsonArray.put(jsonObject);
				}
				if(cursor==null){
					Log.d("Contacts","No Contacts");
				}

				String jsonString = jsonArray.toString();
				/*get the data into the output stream and write the contents (json into the file ) into it. */

				// Create the initial metadata - MIME type and title.
				// Note that the user will be able to change the title later.
				//MetadataChangeSet metadataChangeSet = new MetadataChangeSet.Builder()
				///      .setMimeType("image/jpeg").setTitle("Android Photo.png").build();
				OutputStream outputStream = result.getContents().getOutputStream();
				try {
					outputStream.write(jsonString.getBytes());
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				/*com.google.android.gms.common.api.Status status = file.commitAndCloseContents(
                        getGoogleApiClient(), contentsResult.getContents()).await();
				 */
				MetadataChangeSet changeSet = new MetadataChangeSet.Builder()
				.setTitle("New file")
				.setMimeType("text/plain")
				.setStarred(true).build();

				// Create an intent for the file chooser, and start it.
				IntentSender intentSender = Drive.DriveApi
						.newCreateFileActivityBuilder()
						.setInitialMetadata(changeSet)
						.setInitialContents(result.getContents())
						.build(mGoogleApiClient);
				/* */
				try {
					startIntentSenderForResult(
							intentSender, REQUEST_CODE_CREATOR, null, 0, 0, 0);
				} catch (SendIntentException e) {
					Log.i(TAG, "Failed to launch file chooser.");
				}

			}		
		});
	}

	private void saveMessagesToDrive(){

		//setContentView(R.layout.activity_main);
		//get the uri 

		Drive.DriveApi.newContents(mGoogleApiClient).setResultCallback(new ResultCallback<ContentsResult>() {	


			@Override
			public void onResult(ContentsResult result) {

				// If the operation was not successful, we cannot do anything
				// and must
				// fail.
				if (!result.getStatus().isSuccess()) {
					Log.i(TAG, "Failed to create new contents.");
					return;
				}

				uri =Uri.parse("content://sms");

				//get the projection 
				String[] projection  = {Sms.BODY};

				//get the selection subset
				select = null;

				//get the selectionARGs
				selectionArgs= null;

				//get the sort Order 
				String sortOrder = null;

				//get the contentResolver.
				ContentResolver msgContentResolver = getContentResolver();

				//get the cursor that can be helpful to iterate through the entries.
				Cursor msgCursor = msgContentResolver.query(uri, projection, select, selectionArgs, sortOrder);
				//List<String> messsages = new ArrayList<String>();
				JSONArray jsonArray = new JSONArray();
				JSONObject jsonObject = new JSONObject();

				if(msgCursor.moveToNext()){
					do {
						try {
							jsonObject.put("sms",msgCursor.getString(msgCursor.getColumnIndex(Sms.BODY)));
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} while (msgCursor.moveToNext());
				}
				jsonArray.put(jsonObject);
				String jsonString = jsonArray.toString();

				OutputStream outputStream = result.getContents().getOutputStream();
				try {
					outputStream.write(jsonString.getBytes());
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				/*com.google.android.gms.common.api.Status status = file.commitAndCloseContents(
                getGoogleApiClient(), contentsResult.getContents()).await();
				 */
				MetadataChangeSet changeSet = new MetadataChangeSet.Builder()
				.setTitle("MessagesFile")
				.setMimeType("text/plain")
				.setStarred(true).build();

				// Create an intent for the file chooser, and start it.
				IntentSender intentSender = Drive.DriveApi
						.newCreateFileActivityBuilder()
						.setInitialMetadata(changeSet)
						.setInitialContents(result.getContents())
						.build(mGoogleApiClient);
				/* */
				try {
					startIntentSenderForResult(
							intentSender, REQUEST_CODE_CREATOR, null, 0, 0, 0);
				} catch (SendIntentException e) {
					Log.i(TAG, "Failed to launch file chooser.");
				}

			}		
		});

	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	/*This method serves to maintain communication from fragment to parent activity: 
	 * This method is result of the "communicator" interface we have implemented.
	 */
	public void respond(String source,String destination,String username,String password,boolean connection_req,boolean connection_res){
		long id = databaseManagerAdapter.insert(source, destination, username, password, true,false);
		if(id<0){
			Message.Message(this,"Unable to Insert");
		}else{
			Message.Message(this, "Able to Insert Properly");
			/* Do the select Query and get the data */
			Data data = databaseManagerAdapter.select();
			Log.d("ServiceClient","Source " +data.getSource() +" Destination : "+data.getDestination()+ " Username : "+data.getUsername() +" Password : "+data.getPassword());
			if(source.equals("Photos") && destination.equals("GoogleDrivePhotos")){
				final Intent imageToGoogleDriveIntent = new Intent(getApplicationContext(),ImageServiceToGoogleDrive.class);
				imageToGoogleDriveIntent.putExtra("username", data.getUsername());
				imageToGoogleDriveIntent.putExtra("password",data.getPassword());
				startService(imageToGoogleDriveIntent);
			}	
			else{
				Log.d(TAG,"No Service Called");
			}
			dataSet.add(data);
			arrayAdap.notifyDataSetChanged();
		}
	}




	/*
	 Adding the ConnectionCallBacks, so that we can interact with the GoogleApiClient.
	 And know the status, if the connection is running or the connection is suspended.
	 */

	@Override
	public void onConnected(Bundle connectionHint) {
		// TODO Auto-generated method stub

		Log.i(TAG, "API client connected.");
		/*  if (mBitmapToSave == null) {
	        	// This activity has no UI of its own. Just start the camera.
	            startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE),
	                    REQUEST_CODE_CAPTURE_IMAGE);
	            return;
	        }
	        saveFileToDrive();



		 */
		saveContactsToDrive();
		saveMessagesToDrive();
		//SaveImagesToDrive();
	}

	@Override
	public void onConnectionSuspended(int cause) {
		// TODO Auto-generated method stub
		Log.i(TAG, "GoogleApiClient connection suspended");
	}

	/*Adding onConnectionFailedListener : It helps me getting the status if the connection is failed.  
	 */

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		// TODO Auto-generated method stub

		Log.i(TAG, "GoogleApiClient connection failed: " + result.toString());
		if (!result.hasResolution()) {
			// show the localized error dialog.
			GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this, 0).show();
			return;
		}
		// The failure has a resolution. Resolve it.
		// Called typically when the app is not yet authorized, and an
		// authorization
		// dialog is displayed to the user.
		try {
			result.startResolutionForResult(this, REQUEST_CODE_RESOLUTION);
		} catch (SendIntentException e) {
			Log.e(TAG, "Exception while starting resolution activity", e);
		}
	}
	/*
	ActionBarActivity : 
	The following method helps us create the options for the actions bar.
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main_menu, menu);
		return super.onCreateOptionsMenu(menu);
	} 

	/* The Following Method , helps to select what action should be performed on clicking the particular activity.
	 * (non-Javadoc)
	 * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// action with ID action_refresh was selected
		case R.id.action_add:
			FragmentManager manager = getFragmentManager();
			ServiceDialog sd=new ServiceDialog();
			sd.show(manager,"Test");
			Log.d(TAG,"Add Button Selected on Action Bar");

			break;
			// action with ID action_settings was selected
		case R.id.action_delete:
			Log.d(TAG,"Delete Button Selected on Action Bar");  
			break;
		default:
			Log.d(TAG,"Improper Action Bar Button Selected Selected");  
			break;
		}
		return true;
	}



}
