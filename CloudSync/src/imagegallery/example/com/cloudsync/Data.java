package imagegallery.example.com.cloudsync;

public class Data {
	
	private String username;
	private String password;
	private String source;
	private String destination;
	private boolean connected_req;
	private boolean connected_res;
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public boolean isConnected_req() {
		return connected_req;
	}
	public void setConnected_req(boolean connected_req) {
		this.connected_req = connected_req;
	}
	public boolean isConnected_res() {
		return connected_res;
	}
	public void setConnected_res(boolean connected_res) {
		this.connected_res = connected_res;
	}
}
