package imagegallery.example.com.cloudsync;



import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class PolicyListAdapter extends ArrayAdapter<Data> {
	private final Context context;
	private final ArrayList<Data> dataSet;

	public PolicyListAdapter(Context context, ArrayList<Data> dataSet2) {
		super(context, R.layout.policy_list, dataSet2);
		this.context = context;
		this.dataSet = dataSet2;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.policy_list, parent, false);
		TextView textView = (TextView) rowView.findViewById(R.id.tvUsernameList);
		ImageView imageView1 = (ImageView) rowView.findViewById(R.id.imageView1);
		ImageView imageView2 = (ImageView) rowView.findViewById(R.id.imageView2);

		textView.setText(dataSet.get(position).getUsername());
		String source = dataSet.get(position).getSource();
		String destination = dataSet.get(position).getDestination();

		if(source.equalsIgnoreCase("Photos")){
			imageView1.setImageResource(R.drawable.icon_photo_gallery);
		}

		if(source.equalsIgnoreCase("Videos")){
			imageView1.setImageResource(R.drawable.icon_videos);
		}
		if(source.equalsIgnoreCase("Messages")){
			imageView1.setImageResource(R.drawable.icon_messages);
		}
		if(source.equalsIgnoreCase("BrowserHistory")){
			imageView1.setImageResource(R.drawable.icon_google_chrome);
		}


		if(source.equalsIgnoreCase("Contacts")){
			imageView1.setImageResource(R.drawable.icon_contacts);
		}
		if(source.equalsIgnoreCase("GoogleDrivePhotos") || source.equalsIgnoreCase("GoogleDriveMessages") ||source.equalsIgnoreCase("GoogleDriveContacts") || source.equalsIgnoreCase("GoogleDriveBrowserHistory")  ){
			imageView1.setImageResource(R.drawable.icon_google_drive);
		}
		if(source.equalsIgnoreCase("DropBoxPhotos") || source.equalsIgnoreCase("DropBoxMessages") ||source.equalsIgnoreCase("DropBoxContacts") || source.equalsIgnoreCase("DropBoxBrowserHistory") ){
			imageView1.setImageResource(R.drawable.icon_dropbox);
		}


		if(destination.equalsIgnoreCase("Photos")){
			imageView2.setImageResource(R.drawable.icon_photo_gallery);
		}
		if(destination.equalsIgnoreCase("Videos")){
			imageView2.setImageResource(R.drawable.icon_videos);
		}
		if(destination.equalsIgnoreCase("Messages")){
			imageView2.setImageResource(R.drawable.icon_messages);
		} 
		if(destination.equalsIgnoreCase("BrowserHistory")){
			imageView2.setImageResource(R.drawable.icon_google_chrome);
		}
		if(destination.equalsIgnoreCase("Contacts")){
			imageView2.setImageResource(R.drawable.icon_contacts);
		}
		if(destination.equalsIgnoreCase("GoogleDrivePhotos") || destination.equalsIgnoreCase("GoogleDriveMessages") ||destination.equalsIgnoreCase("GoogleDriveContacts") || destination.equalsIgnoreCase("GoogleDriveBrowserHistory")  ){
			imageView2.setImageResource(R.drawable.icon_google_drive);
		}
		if(destination.equalsIgnoreCase("DropBoxPhotos") || destination.equalsIgnoreCase("DropBoxMessages") ||destination.equalsIgnoreCase("DropBoxContacts") || destination.equalsIgnoreCase("DropBoxBrowserHistory") ){
			imageView2.setImageResource(R.drawable.icon_dropbox);
		}
			
		return rowView;
	}
} 

