package imagegallery.example.com.cloudsync;

import com.google.android.gms.internal.cu;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.style.SuperscriptSpan;
import android.util.Log;

public class DatabaseManagerAdapter {
	Data data;
	DatabaseManager databaseManager;
	public DatabaseManagerAdapter(Context context){
		databaseManager  = new DatabaseManager(context);
		data = new Data();
	}
	
	public long insert(String source,String destination,String username,String password,boolean connected_req,boolean connected_res){

		SQLiteDatabase db = databaseManager.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put(DatabaseManager.SOURCE, source);
		contentValues.put(DatabaseManager.DESTINATION, destination);
		contentValues.put(DatabaseManager.USERNAME, username);
		contentValues.put(DatabaseManager.PASSWORD,password);
		contentValues.put(DatabaseManager.CONNECTED_REQ,connected_req);
		contentValues.put(DatabaseManager.CONNECTED_RES,connected_res);
		long id = db.insert(DatabaseManager.TABLE_NAME, null, contentValues);
		return id;
	}

	public int update(String source,String destination,String username,String password,String connected_res,String connected_req){
		int count = -1;

		return count;
	}

	public int delete(String source,String destination,String username,String password,String connected_res,String connected_req){
		int count = -1;

		return count;
	}

	//public int select(String source,String destination,String username,String password,String connected_res,String connected_req)
	public Data select(){
		int count = -1;
		SQLiteDatabase db = databaseManager.getWritableDatabase();
		
		String [] columns = {DatabaseManager.SOURCE,DatabaseManager.DESTINATION,DatabaseManager.USERNAME,DatabaseManager.PASSWORD,DatabaseManager.CONNECTED_REQ,DatabaseManager.CONNECTED_RES};
		//String [] selectionArgs ={source,username,password,connected_req,connected_res};
		//Cursor cursor = db.query(DatabaseManager.TABLE_NAME, columns, selection, selectionArgs, null, null, null);
		
		Cursor cursor = db.rawQuery("SELECT * FROM  " +DatabaseManager.TABLE_NAME +"", null);
		if(cursor!=null){
		cursor.moveToPosition(cursor.getCount() - 1);
		
		String source = cursor.getString(cursor.getColumnIndex(DatabaseManager.SOURCE));
		data.setSource(source);
		String destination = cursor.getString(cursor.getColumnIndex(DatabaseManager.DESTINATION));
		data.setDestination(destination);
		String username = cursor.getString(cursor.getColumnIndex(DatabaseManager.USERNAME));
		data.setUsername(username);
		String password = cursor.getString(cursor.getColumnIndex(DatabaseManager.PASSWORD));
		data.setPassword(password);
		Log.d("DatabaseManager","Source : " +source+"Destination : "+destination + "username : "+username +"password : "+ password );
		return data ;
		}
		return null;
	}

	static class DatabaseManager extends SQLiteOpenHelper {

		private static final String TAG = "DatabaseManager" ;
		/*Database Related Tabs */
		private static final String DATABASE_NAME ="CloudSyncDatabase";
		private static final String TABLE_NAME="PolicyTable";
		private static final int DATABASE_VERSION = 5;

		/* Database Table's Column Structure*/
		private static final String UID = "_id";
		private static final String SOURCE = "source";
		private static final String DESTINATION = "destination";
		private static final String USERNAME = "username";
		private static final String PASSWORD = "password";
		private static final String CONNECTED_REQ = "connected_req";
		private static final String CONNECTED_RES = "connected_res";

		/* Create Table */
		private static final String CREATE_TABLE="CREATE TABLE  "+TABLE_NAME+
				"(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
				SOURCE+" TEXT, " +
				DESTINATION+" TEXT, " +
				USERNAME+" TEXT, "+
				PASSWORD+" TEXT, "+
				CONNECTED_REQ+" BOOLEAN, "+
				CONNECTED_RES+" BOOLEAN"+
				" );";

		/*Drop Table */
		private static final String DROP_TABLE="DROP TABLE IF EXISTS "+TABLE_NAME;


		Context context;
		public DatabaseManager(Context context){
			super(context,DATABASE_NAME,null,DATABASE_VERSION);
			this.context = context;
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			// TODO Auto-generated method stub
			try{
				db.execSQL(CREATE_TABLE);
			}catch(SQLException e){
				Message.Message(this.context, "Unable to create table something went wrong");
				Log.d(TAG,"Unable to create table something went wrong");
				e.printStackTrace();
			}

			Log.d(TAG,"Table created Successfully");
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub
			try{
				db.execSQL(DROP_TABLE);
				db.execSQL(CREATE_TABLE);
			}catch(SQLException e){
				e.printStackTrace();
			}
		}


	}

}

