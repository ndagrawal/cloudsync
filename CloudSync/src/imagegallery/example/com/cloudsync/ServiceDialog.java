package imagegallery.example.com.cloudsync;

import com.google.android.gms.drive.internal.ac;
import com.google.android.gms.internal.da;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class ServiceDialog extends DialogFragment implements View.OnClickListener{

	private static final String TAG = "ServiceDialog";
	EditText etUserName;
	EditText etPassword;
	Spinner sResourceName;
	Spinner sDestinationName;
	Button bStartService;
	Button bStopService;
	DatabaseManagerAdapter databaseManagerAdapter;
	communicator comm;
	/*Remember never ever initialise the UI Elements of the fragment in the OnCreate Method. */
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		
	}

	/* Remember always initialise the UI Elements of the fragment in the OnCreateView Method 
	 * as the Dialog's UI Get initialised here, once the Dialog is initiliased you can initialise the UI Elements Within it.
	 * (non-Javadoc)
	 * @see android.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 */
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View V =inflater.inflate(R.layout.activity_dialog, null);
		etUserName = (EditText)V.findViewById(R.id.etServiceUsername);
		etPassword = (EditText)V.findViewById(R.id.etServicePassword);
		sResourceName = (Spinner)V.findViewById(R.id.spinnerResourceName);
		sDestinationName = (Spinner)V.findViewById(R.id.spinnerDestinationName);
		bStartService = (Button)V.findViewById(R.id.bStartService);
		bStopService = (Button)V.findViewById(R.id.bStopService);
		getDialog().setTitle("Set Policy");
		//databaseManagerAdapter = new DatabaseManagerAdapter(getActivity());
		bStartService.setOnClickListener(this);
		bStopService.setOnClickListener(this);
		return V;
	}

	
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		comm = (communicator)activity;
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		switch(v.getId()){	
		case R.id.bStartService:
			Log.d(TAG,"Start Service button was selected");
			
			/*if new policy then: 
			 * 1. Save the policy to the database 
			 * 2. Policy status = true in the Policy Table
			 * 3. Startuploading the Data*/
			String username = etUserName.getText().toString();
			String password = etPassword.getText().toString();
			String source = sResourceName.getSelectedItem().toString();
			String destination = sDestinationName.getSelectedItem().toString();
			comm.respond(source, destination, username, password, true, false);
			
			
			//ServiceClient serviceClient = ServiceClient.getInstance();
			//serviceClient.updateListView("hu");
			 /*if existing policy in database,
			 * 1.update the policy status = true if untrue
			 * 2. Startuploading the Data 
			 * 3. */
			
			
			/*if existing policy in database,
			 * 1. and if policy status = true in policyTable then prompt user policy is running.
			 */
			
			break;
		case R.id.bStopService:
			Log.d(TAG,"Stop Service button was selected");
			/*1. Change (update) Policy status = false in the Policy Table
			 *2. StopUploding the Data */

			break;
		default:
			Log.d(TAG,"No button was selected");
		}
		
	}
 
	
	
	
}
